import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';
import { MustMatch } from 'src/helpers/MustMatch';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private service: UserService,
    private route: Router
  ) { }


  ngOnInit(): void {
    if(localStorage.getItem('token') != null){
      this.route.navigate(['/payment']);
    }
    this.initForm();
  }

  get f() { 
    return this.form.controls; 
  }


  initForm() {
    this.form = this.fb.group({
      FullName: ['', Validators.required],
      UserName: ['',Validators.required],
      Email: ['',[Validators.required, Validators.email]],
      Password: ['',[Validators.required, Validators.minLength(6)]], 
      ConfirmPassword: ['', Validators.required]
    },{
      validators: MustMatch('Password', 'ConfirmPassword')
    });
  }

  onSubmit() {
    this.submitted = true;
    let erroMessage: string[] = [];
    // stop here if form is invalid
    if (this.form.invalid) {
        return;
    }
    const {ConfirmPassword, ...data} = this.form.value;

    this.service.registerUser(data).subscribe((res: any) =>{
      if(res && res.Succeeded === true){
        alert('Register User Success !')
        this.form.reset();
      }
      else{
       if(res.Errors.length > 0){
          res.Errors.forEach(erro => {
           erroMessage.push(erro.Description + ',')
         });
       }
       alert(erroMessage)
      }
    });
}
}
