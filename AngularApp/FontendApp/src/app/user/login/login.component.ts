import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';
import { MustMatch } from 'src/helpers/MustMatch';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private service: UserService,
    private router: Router
  ) { }


  ngOnInit(): void {
    this.initForm();
  }

  get f() { 
    return this.form.controls; 
  }


  initForm() {
    this.form = this.fb.group({
      UserName: ['',Validators.required],
      Password: ['',[Validators.required, Validators.minLength(6)]], 
    });
  }

  onSubmit() {
    this.submitted = true;
    let erroMessage: string[] = [];
    // stop here if form is invalid
    if (this.form.invalid) {
        return;
    }
    const {...data} = this.form.value;

    this.service.login(data).subscribe((res: any) =>{
      alert('Login Success');
      localStorage.setItem('token', res.token);
      this.router.navigateByUrl('/payment')
    },
    erro =>{
        if(erro.status == 400){
          alert(erro.error.message + ' Authentication Failed!');
        }
      }
    );
}

}
