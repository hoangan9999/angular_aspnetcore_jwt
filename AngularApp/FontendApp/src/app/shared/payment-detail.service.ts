import { Injectable } from '@angular/core';
import { PaymentDetail } from './payment-detail.model';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentDetailService {

  paymentDetailList: BehaviorSubject<Array<any>> = new BehaviorSubject([]);

  paymentDetailData = this.paymentDetailList.asObservable();

  readonly rootUrl = "http://localhost:25273/api";

  constructor(private http: HttpClient) { }

  postPaymentDetail(form: PaymentDetail){
    return this.http.post(this.rootUrl + '/paymentdetail',form);
  }

  getAllPaymentDetail(){
    return this.http.get(this.rootUrl + '/paymentdetail');
  }

  deletePaymentDetail(id){
    return this.http.delete(this.rootUrl + '/paymentdetail/'+id, id);
  }

  refeshPaymentDetailList(){
    this.getAllPaymentDetail().subscribe((res: any) =>{
      this.paymentDetailList.next(res);
    });
  }

}
