import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly rootUrl = "http://localhost:25273/api";

  constructor(private http: HttpClient) { }


  registerUser(form){
    return this.http.post(this.rootUrl + '/ApplicationUser/Register', form);
  }

  login(form){
    return this.http.post(this.rootUrl + '/ApplicationUser/login', form)
  }

  getUserProfile(){
    //var tokenHeader = new HttpHeaders({'Authorization' : 'Bearer ' + localStorage.getItem('token')});
    return this.http.get(this.rootUrl + '/UserProfile');
  }

}
