import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { error } from 'protractor';
import { PaymentDetailService } from 'src/app/shared/payment-detail.service';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: []
})
export class PaymentDetailComponent implements OnInit {

  get CardOwnerNameControl() {
    return this.form.get('CardOwnerName');
  }

  get CardNumberControl(){
    return this.form.get('CardNumber');
  }

  get ExpirationDateControl(){
    return this.form.get('ExpirationDate');
  }

  get CVVControl(){
    return this.form.get('CVV');
  }

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private service: PaymentDetailService
  ) { }


  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      PMId: 0,
      CardOwnerName: ['',Validators.required],
      CardNumber: ['',[Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
      ExpirationDate: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
      CVV: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(3)]],
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.service.postPaymentDetail(this.form.value).subscribe(res =>{
      if(res){
        this.form.reset();
        this.service.refeshPaymentDetailList();
        alert("Post Success");
      }
      else{
        alert('Fail');
      }
    },
    error =>{
     if(error.status == 403){
      alert('You dont have permission');
     }
    }
    );
    
  }

}
