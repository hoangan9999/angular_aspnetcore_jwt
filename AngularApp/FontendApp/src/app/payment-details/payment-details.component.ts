import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: []
})
export class PaymentDetailsComponent implements OnInit {

  userDetail ={

  }

  constructor(private router: Router, private server: UserService) { }

  ngOnInit(): void {
    this.server.getUserProfile().subscribe(
      res =>{
        this.userDetail = res
      },
      erro =>{
        console.log(erro);
      }
    );
  }

  logout(){
    localStorage.removeItem('token');
    alert('Logout Success');
    this.router.navigate(['/user/login']);
  }

}
