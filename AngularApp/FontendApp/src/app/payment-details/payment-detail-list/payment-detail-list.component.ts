import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { from, observable, of } from 'rxjs';
import { PaymentDetailService } from 'src/app/shared/payment-detail.service';

@Component({
  selector: 'app-payment-detail-list',
  templateUrl: './payment-detail-list.component.html',
  styleUrls: []
})
export class PaymentDetailListComponent implements OnInit, OnChanges {
  @Input() userDetail;
  
  paymentDetailList = [];

  constructor(private service: PaymentDetailService) { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.userDetail)
  }

  

  ngOnInit(): void {
    this.getListPaymentDetail();
  }



  getListPaymentDetail(){
    this.service.refeshPaymentDetailList();
    this.service.paymentDetailData.subscribe(res =>{
      this.paymentDetailList = res;
    });
  }

  delete(id,index){
    let confirm = window.confirm(`Do you want to delete index ${index + 1} ?`);
    if(confirm){
      this.service.deletePaymentDetail(id).subscribe(res =>{
        if(res){
          alert(`delete payment index ${index + 1} success`);
          this.service.refeshPaymentDetailList()
        }
      });
    }
    else{

    }

   
  }
  
}
