﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class PaymentDetailContext: IdentityDbContext
    {
      
        
        public PaymentDetailContext(DbContextOptions options): base(options)
        {

        }

        public DbSet<PaymentDetail> PaymentDetails { get; set; }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}
